/* 
 * animation des éléments de la page
 */

// Varible de position et de vitesse des taches de couleur sur le fond d'ecran
var blue = {"posX":0,"posY":0,"speedX":1,"speedY":4};
var yellow = {"posX":0,"posY":0,"speedX":-15,"speedY":-10};
var pink = {"posX":0,"posY":0,"speedX":1,"speedY":9};

var isMobile = false;
var page = "";

$(document).ready(function(){
    // Initialisation
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){
        isMobile = true;
    }
    initBackground();
    if($('#logo').hasClass('large')){
        setInterval(refreshLogoSize,1000/60);
    }else{
        refreshPosition();
        $(window).resize(refreshPosition);
        setInterval(moveBackground,1000/60);
        $("#header").addClass("bgWhite");
    }
    $(".projectLink").on("mouseover",showDescription);
    $(".projectLink").on("mouseout",hideDescription); 
    $(".imgLink").on("mouseover",showTag);
    $(".imgLink").on("mouseout",hideTag);
    if($(window).width() <= 480 || isMobile){
        $(".imgLink").children('div').css("display","flex");
    }
    $(window).on("resize",allTags);
    $("#menu").on("click",showLeftNav);
    lockNav();
});

function initBackground(){
    // Récupère les positions des éléments du fond en pourcentage
    blue.posY = parseFloat($("#blue").css("top").substr(0,$("#blue").css("top").length-2)) / $(window).height() * 100;
    blue.posX = parseFloat($("#blue").css("left").substr(0,$("#blue").css("left").length-2)) / $(window).width() * 100;
    yellow.posY = parseFloat($("#yellow").css("top").substr(0,$("#yellow").css("top").length-2)) / $(window).height() * 100;
    yellow.posX = parseFloat($("#yellow").css("left").substr(0,$("#yellow").css("left").length-2)) / $(window).width() * 100;
    pink.posY = parseFloat($("#pink").css("top").substr(0,$("#pink").css("top").length-2)) / $(window).height() * 100;
    pink.posX = parseFloat($("#pink").css("left").substr(0,$("#pink").css("left").length-2)) / $(window).width() * 100;
}

function refreshLogoSize(){
    // Change la taille du logo en fonction de la position du scroll
    if($(window).scrollTop() > 0){
        $("#logo").removeClass("large");
        $("#logo").addClass("small");
        $("#header").addClass("bgWhite");
    }else{
        $("#logo").removeClass("small");
        $("#logo").addClass("large");
        $("#header").removeClass("bgWhite");
    }
    refreshPosition();
}

function refreshPosition(){
    // change la position de la barre de navigation en fonction de la taille de la fenêtre et du logo
    if($(window).width() > 480 && !isMobile){
        $("#leftNav").css('width',$("main").width()*0.2);
        $("#leftNav").css('top',$("#header").height());
    }else{
        $("#leftNav").css('width',$("main").width()*0.7);
        $("#leftNav").css('height',$(window).height()-48);
        $("#leftNav").css('top',0);
    }
    if(isMobile){
        $("#leftNav").css('height',$(window).height());
    }
    $("#galery,#project").css('margin-top',$("#header").height());
    if($(window).width() > 1400){
        $("#leftNav").css('left',($(window).width() - 1250)/2);
    }else{
        $("#leftNav").css('left',0);
    }
    moveBackground();
}

function moveBackground(){
    // Déplace les éléments du fond en fonction de la position du scroll de la page
    var scroll = 1-($(document).height() - $(document).scrollTop() - $(window).height())/($(document).height()-$(window).height());
    $("#blue").css("top",blue.speedY*scroll+blue.posY+"%");
    $("#blue").css("left",blue.speedX*scroll+blue.posX+"%");
    $("#yellow").css("top",yellow.speedY*scroll+yellow.posY+"%");
    $("#yellow").css("left",yellow.speedX*scroll+yellow.posX+"%");
    $("#pink").css("top",pink.speedY*scroll+pink.posY+"%");
    $("#pink").css("left",pink.speedX*scroll+pink.posX+"%");
 
}

function showDescription(){
    // Montre la description
    if($(window).width() > 480  && !isMobile){
        $(this).next("p").show();
    }
}

function hideDescription(){
    // Cache la description
    if($(window).width() > 480  && !isMobile){
        if($(this).next("p").attr('id') !== page){
            $(this).next("p").hide();
        }
        
    }
}

function showTag(){
    // Affiche le tag d'une image de la galerie
    $(this).children("div").css("display","flex");
}

function hideTag(){
    // Cache le tag d'une image de la galerie
    if($(window).width() > 480 && !isMobile){
        $(this).children("div").hide();
    }
}

function allTags(){
    // Affiche ou cache tous les tags en fonction de la taille de l'ecran et cache leftNav sur les petit ecran
    if($(window).width() > 480 && !isMobile){
        $(".imgLink").children('div').hide();
        $('#leftNav').show();
        $(".projectLink").next("p").hide();
    }else{
        $(".imgLink").children('div').css("display","flex");
        $('#leftNav').hide();
    }
    $('.greyScreen').remove();
}

function showLeftNav(){
    // Affiche leftNav quand on clique sur le menu
    $('#leftNav').show();
    $('#leftNav').after("<div class='greyScreen'></div>");
    $(".greyScreen").on("click",hideLeftNav);
    $(".projectLink").next("p").show();
}

function hideLeftNav(){
    $('#leftNav').hide();
    $('.greyScreen').remove();
}

function lockNav(){
    var url_string = window.location.href;
    var url = new URL(url_string);
    page = url.searchParams.get("page");
    var id = "#"+page;
    $(id).show();
}