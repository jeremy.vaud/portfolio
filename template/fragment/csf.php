<!-- csf -->

<div id="project">
    <div class="company">
        <img src="img/logo/Logo_CSF.jpg"/>
        <p>La CSF est un regroupement d’associations œuvrant pour les familles de manière générale, de l’enfant au retraité.</p>
    </div>
    <div class="picto">
        <img src="img/picto/Picto_Web.svg"/>       
    </div>
    <h2>Illustrations</h2>
    <h3>2015</h3>
    <p>Illustrations vectorielles pour les besoins de l’association.</p>
    <img src="img/project/csf/Maquette-site_Illustrations_01.jpg"/>
    <img src="img/project/csf/Maquette-site_Illustrations_02.jpg"/>
    <img src="img/project/csf/Maquette-site_Illustrations_03.jpg"/>
    <img src="img/project/csf/Maquette-site_Illustrations_04.jpg"/>
    <img src="img/project/csf/Maquette-site_Illustrations_05.jpg"/>
</div>

