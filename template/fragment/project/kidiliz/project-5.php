<!-- projet kidiliz 1: Tote bag de Noël -->

<div id="project">
    <?=$projectButton?>
    <div class="company">
        <img src="img/logo/Logo_KidilizGroup.svg"/>
        <p>Anciennement Groupe Zannier, Kidiliz était jusqu’à fin 2020 une référence dans le monde du prêt-à-porter pour enfant en France ; à sa proue Catimini, Absorba, Chipie et des licences telles que Kenzo Kids ou G-Star Raw en vente sur leur e-shop ou en magasin dont leur enseigne Kidiliz.</p>
    </div>
    <div class="picto">
        <img src="img/picto/Picto_Print.svg"/>       
    </div>
    <h2>Tote bag de Noël</h2>
    <h3>Automne-Hiver 2020</h3>
    <p>Conception graphique de la sérigraphie.</p>
    <img src="img/project/kidiliz-5/ToteBag_01.jpg"/>
    <img src="img/project/kidiliz-5/ToteBag_02.jpg"/>
</div>

