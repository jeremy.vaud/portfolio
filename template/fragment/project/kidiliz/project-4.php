<!-- projet kidiliz 4: Vitrine Color Block -->

<div id="project">
    <?=$projectButton?>
    <div class="company">
        <img src="img/logo/Logo_KidilizGroup.svg"/>
        <p>Anciennement Groupe Zannier, Kidiliz était jusqu’à fin 2020 une référence dans le monde du prêt-à-porter pour enfant en France ; à sa proue Catimini, Absorba, Chipie et des licences telles que Kenzo Kids ou G-Star Raw en vente sur leur e-shop ou en magasin dont leur enseigne Kidiliz.</p>
    </div>
    <div class="picto">
        <img src="img/picto/Picto_Print.svg"/>
        <img src="img/picto/Picto_Web.svg"/>        
    </div>
    <h2>Vitrine Color Block</h2>
    <h3>Printemps-Été 2020</h3>
    <p>Scénographie et conception des supports vitrine avec relais sur le web et les réseaux sociaux.</p>
    <img src="img/project/kidiliz-4/VitrineColorBlock_01.jpg"/>
    <img src="img/project/kidiliz-4/VitrineColorBlock_02.jpg"/>
    <img src="img/project/kidiliz-4/VitrineColorBlock_03.jpg"/>
</div>

