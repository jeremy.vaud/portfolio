<!-- projet kidiliz 1: Workbook Chipie -->

<div id="project">
    <?=$projectButton?>
    <div class="company">
        <img src="img/logo/Logo_KidilizGroup.svg"/>
        <p>Anciennement Groupe Zannier, Kidiliz était jusqu’à fin 2020 une référence dans le monde du prêt-à-porter pour enfant en France ; à sa proue Catimini, Absorba, Chipie et des licences telles que Kenzo Kids ou G-Star Raw en vente sur leur e-shop ou en magasin dont leur enseigne Kidiliz.</p>
    </div>
    <div class="picto">
        <img src="img/picto/Picto_Print.svg"/>       
    </div>
    <h2>Workbook Chipie</h2>
    <h3>Printemps-Été 2020</h3>
    <p>Mise en page du manuel de collection pour la marque Chipie.</p>
    <img src="img/project/kidiliz-1/WB-Chipie_01.jpg"/>
    <img src="img/project/kidiliz-1/WB-Chipie_02.jpg"/>
    <img src="img/project/kidiliz-1/WB-Chipie_03.jpg"/>
</div>

