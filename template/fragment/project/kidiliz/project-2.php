<!-- projet kidiliz 2: Pictogrammes -->

<div id="project">
    <?=$projectButton?>
    <div class="company">
        <img src="img/logo/Logo_KidilizGroup.svg"/>
        <p>Anciennement Groupe Zannier, Kidiliz était jusqu’à fin 2020 une référence dans le monde du prêt-à-porter pour enfant en France ; à sa proue Catimini, Absorba, Chipie et des licences telles que Kenzo Kids ou G-Star Raw en vente sur leur e-shop ou en magasin dont leur enseigne Kidiliz.</p>
    </div>
    <div class="picto">
        <img src="img/picto/Picto_Web.svg"/>       
    </div>
    <h2>Pictogrammes</h2>
    <h3>Plateforme de vente B2B</h3>
    <p>Design de l’ensemble des pictogrammes à destination de la plateforme de vente en ligne du groupe en B2B.</p>
    <img src="img/project/kidiliz-2/Pictogrammes_01.jpg"/>
</div>

