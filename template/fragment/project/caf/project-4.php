<!-- caf 4: “La Semaine de la parentalité” -->

<div id="project">
    <?=$projectButton?>
    <div class="company">
        <img src="img/logo/Logo_Caf.svg"/>
        <p>La Caisse d’allocations familiales de Clermont-Ferrand (63) se fait à la fois relais des opérations de communication nationale, mais produit aussi ses propres ressources à visée locale.</p>
    </div>
    <div class="picto">
        <img src="img/picto/Picto_Print.svg"/>       
        <img src="img/picto/Picto_Web.svg"/>       
    </div>
    <h2>La Semaine de la parentalité</h2>
    <h3>2017</h3>
    <p>Conception des supports de communication pour les divers événements de la Semaine de la parentalité organisée par la Caf du Puy-de-Dôme.</p>
    <img src="img/project/caf-4/SemaineParentalite_01.jpg"/>
    <img src="img/project/caf-4/SemaineParentalite_02.jpg"/>
    <img src="img/project/caf-4/SemaineParentalite_03.jpg"/>
    <img src="img/project/caf-4/SemaineParentalite_04.jpg"/>
</div>

