<!-- caf 2: “Être locataire” -->

<div id="project">
    <?=$projectButton?>
    <div class="company">
        <img src="img/logo/Logo_Caf.svg"/>
        <p>La Caisse d’allocations familiales de Clermont-Ferrand (63) se fait à la fois relais des opérations de communication nationale, mais produit aussi ses propres ressources à visée locale.</p>
    </div>
    <div class="picto">
        <img src="img/picto/Picto_Print.svg"/>       
    </div>
    <h2>“Être locataire”</h2>
    <h3>Guide pratique</h3>
    <p>Mise en page du livret à destination des allocataires de la Caf du Puy-de-Dôme.</p>
    <img src="img/project/caf-2/LivretLocataire_01.jpg"/>
    <img src="img/project/caf-2/LivretLocataire_02.jpg"/>
    <img src="img/project/caf-2/LivretLocataire_03.jpg"/>
</div>

