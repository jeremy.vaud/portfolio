<!-- caf 1: “Être locataire” -->

<div id="project">
    <?=$projectButton?>
    <div class="company">
        <img src="img/logo/Logo_Caf.svg"/>
        <p>La Caisse d’allocations familiales de Clermont-Ferrand (63) se fait à la fois relais des opérations de communication nationale, mais produit aussi ses propres ressources à visée locale.</p>
    </div>
    <div class="picto">
        <img src="img/picto/Picto_Print.svg"/>       
    </div>
    <h2>Rapport d’activité</h2>
    <h3>2016</h3>
    <p>Mise en page du rapport d’activité de l’année 2016.</p>
    <img src="img/project/caf-1/RapportActiviteCAF_01.jpg"/>
    <img src="img/project/caf-1/RapportActiviteCAF_02.jpg"/>
    <img src="img/project/caf-1/RapportActiviteCAF_03.jpg"/>
</div>

