<!-- caf 3: “Projet’toi” -->

<div id="project">
    <?=$projectButton?>
    <div class="company">
        <img src="img/logo/Logo_Caf.svg"/>
        <p>La Caisse d’allocations familiales de Clermont-Ferrand (63) se fait à la fois relais des opérations de communication nationale, mais produit aussi ses propres ressources à visée locale.</p>
    </div>
    <div class="picto">
        <img src="img/picto/Picto_Print.svg"/>       
        <img src="img/picto/Picto_Web.svg"/>       
    </div>
    <h2>“Projet’toi”</h2>
    <h3>Identité graphique</h3>
    <p>Conception d’une identité graphique pour le dispositif Projet’toi de la Caf du Puy-de-Dôme.</p>
    <img src="img/project/caf-3/Maquette-site_Projettoi_01.jpg"/>
    <img src="img/project/caf-3/Maquette-site_Projettoi_02.jpg"/>
</div>

