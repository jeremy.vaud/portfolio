<!-- projet valtex 1: Catalogue Daycollection -->

<div id="project">
    <?=$projectButton?>
    <div class="company">
        <img src="img/logo/Logo_Valtex.svg"/>
        <p>Spécialiste en impression textile, l’entreprise débute son activité par la gravure de cylindres d’impression, elle se diversifie ensuite avec l’impression numérique puis utilise ses savoir-faire pour créer en 2012 deux marques de décoration textile : Daycollection et Un rendez-vous français.</p>
    </div>
    <div class="picto">
        <img src="img/picto/Picto_Print.svg"/>       
    </div>
    <h2>Catalogue Daycollection</h2>
    <h3>2013-2014</h3>
    <p>Mise en page du catalogue de la marque Daycollection pour l’hiver 2013-2014.</p>
    <img src="img/project/valtex-1/CatalogueDaycollection_01.jpg" alt=""/>
    <img src="img/project/valtex-1/CatalogueDaycollection_02.jpg" alt=""/>
    <img src="img/project/valtex-1/CatalogueDaycollection_03.jpg" alt=""/>   
</div>

