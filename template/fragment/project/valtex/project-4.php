<!-- projet valtex 4: Site web Daycollection -->

<div id="project">
    <?=$projectButton?>
    <div class="company">
        <img src="img/logo/Logo_Valtex.svg"/>
        <p>Spécialiste en impression textile, l’entreprise débute son activité par la gravure de cylindres d’impression, elle se diversifie ensuite avec l’impression numérique puis utilise ses savoir-faire pour créer en 2012 deux marques de décoration textile : Daycollection et Un rendez-vous français.</p>
    </div>
    <div class="picto">
        <img src="img/picto/Picto_Web.svg"/>       
    </div>
    <h2>Site web Daycollection</h2>
    <h3>Bannières</h3>
    <p>Création de bannières et autres visuels pour alimenter le site vitrine de la marque.</p>
    <img src="img/project/valtex-4/SiteDaycollection_01.jpg"/>
    <img src="img/project/valtex-4/SiteDaycollection_02.jpg"/>
    <img src="img/project/valtex-4/SiteDaycollection_03.jpg"/>
    <img src="img/project/valtex-4/SiteDaycollection_04.jpg"/>
    <img src="img/project/valtex-4/SiteDaycollection_05.jpg"/>
    <img src="img/project/valtex-4/SiteDaycollection_06.jpg"/>
</div>

