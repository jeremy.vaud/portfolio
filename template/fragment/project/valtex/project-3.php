<!-- projet valtex 3: Un rendez-vous élégant -->

<div id="project">
    <?=$projectButton?>
    <div class="company">
        <img src="img/logo/Logo_Valtex.svg"/>
        <p>Spécialiste en impression textile, l’entreprise débute son activité par la gravure de cylindres d’impression, elle se diversifie ensuite avec l’impression numérique puis utilise ses savoir-faire pour créer en 2012 deux marques de décoration textile : Daycollection et Un rendez-vous français.</p>
    </div>
    <div class="picto">
        <img src="img/picto/Picto_Print.svg"/>       
    </div>
    <h2>Un rendez-vous élégant</h2>
    <h3>Impression textile</h3>
    <p>Conceptions graphiques pour une impression numérique sur pochettes et foulards.</p>
    <img src="img/project/valtex-3/Maquette-site_UnRendezVousElegant_01.jpg"/>
    <img src="img/project/valtex-3/Maquette-site_UnRendezVousElegant_02.jpg"/>
    <img src="img/project/valtex-3/Maquette-site_UnRendezVousElegant_03.jpg"/>
</div>

