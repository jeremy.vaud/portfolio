<!-- projet valtex 2: Un rendez-vous français -->

<div id="project">
    <?=$projectButton?>
    <div class="company">
        <img src="img/logo/Logo_Valtex.svg"/>
        <p>Spécialiste en impression textile, l’entreprise débute son activité par la gravure de cylindres d’impression, elle se diversifie ensuite avec l’impression numérique puis utilise ses savoir-faire pour créer en 2012 deux marques de décoration textile : Daycollection et Un rendez-vous français.</p>
    </div>
    <div class="picto">
        <img src="img/picto/Picto_Print.svg"/>       
    </div>
    <h2>Un rendez-vous français</h2>
    <h3>Catalogue 2013-2014</h3>
    <p>Mise en page du catalogue de la marque Un rendez-vous français pour l’hiver 2013-2014.</p>
    <img src="img/project/valtex-2/Maquette-site_CatalogueUnRdvFr_01.jpg"/>
    <img src="img/project/valtex-2/Maquette-site_CatalogueUnRdvFr_02.jpg"/>
    <img src="img/project/valtex-2/Maquette-site_CatalogueUnRdvFr_03.jpg"/>
</div>

