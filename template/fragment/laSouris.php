<!-- la souris aprivoisé -->

<div id="project">
    <div class="company">
        <img src="img/logo/Logo_LaSouris.svg"/>
        <p>Michel Vaud, auto-entrepreneur, est le MacGyver du matériel informatique, il opère sur Saint-Etienne et ses alentours.</p>
    </div>
    <div class="picto">
        <img src="img/picto/Picto_IV.svg"/>       
    </div>
    <h2>La souris apprivoisée</h2>
    <h3>logo et carte de visite</h3>
    <p>Design du logo et de la carte de visite de La souris apprivoisée, micro-entreprise de réparation informatique.</p>
    <img src="img/project/laSouris/LaSourisApprivoisee_01.jpg"/>
    <img src="img/project/laSouris/LaSourisApprivoisee_02.jpg"/>
</div>

