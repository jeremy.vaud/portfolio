<!-- kidiliz -->

<div id="galery">
    <a href="index.php?page=kidiliz&project=3" class="imgLink">
        <img src="img/kidiliz-3.jpg"/>
        <div>
            <img src="img/logo/Logo_Kidiliz.svg" class="width45"/>
            <h2>Vitrine G-Star Raw</h2>
            <h3>Printemps-Été 2020</h3>
            <p>Print Web<p>
        </div>
    </a>
    <a href="index.php?page=kidiliz&project=2" class="imgLink">
        <img src="img/kidiliz-2.jpg"/>
        <div>
            <img src="img/logo/Logo_KidilizGroup.svg" class="width45"/>
            <h2>Pictogrammes</h2>
            <h3>Plateforme de vente B2B</h3>
            <p>Web<p>
        </div>
    </a>
    <a href="index.php?page=kidiliz&project=1" class="imgLink">
        <img src="img/kidiliz-1.jpg"/>
        <div>
            <img src="img/logo/Logo_Chipie.svg"  class="width45"/>
            <h2>Workbook Chipie</h2>
            <h3>Printemps-Été 2020</h3>
            <p>Print<p>
        </div>
    </a>
    <a href="index.php?page=kidiliz&project=4" class="imgLink">
        <img src="img/kidiliz-4.jpg"/>
        <div>
            <img src="img/logo/Logo_Kidiliz.svg" class="width45"/>
            <h2>Vitrine Color Block</h2>
            <h3>Printemps-Été 2020</h3>
            <p>Print Web<p>
        </div>
    </a>
    <a href="index.php?page=kidiliz&project=5" class="imgLink">
        <img src="img/kidiliz-5.jpg"/>
        <div>
            <img src="img/logo/Logo_Kidiliz.svg" class="width45"/>
            <h2>Tote bag de Noël</h2>
            <h3>Automne-Hiver 2020</h3>
            <p>Print<p>
        </div>
    </a>
</div>

