<!-- boutons de navigation entre projets -->

<div class="button">
    <a href="index.php?page=<?=$page?>&project=<?=$prev?>" class="btnPrev">
        <img src="img/picto/Picto_Left.svg"/>
        <span>projet<br>précédent</span>
    </a>
    <a href="index.php?page=<?=$page?>&project=<?=$next?>" class="btnNext">
        <span>projet<br>suivant</span>
        <img src="img/picto/Picto_Right.svg"/>       
    </a>
</div>