<!-- caf -->

<div id="galery">
    <a href="index.php?page=caf&project=2" class="imgLink">
        <img src="img/caf-2.jpg"/>
        <div>
            <img src="img/logo/Logo_Caf.svg" class="width40"/>
            <h2>“Être locataire”</h2>
            <h3>Guide pratique</h3>
            <p>Print<p>
        </div>
    </a>
    <a href="index.php?page=caf&project=1" class="imgLink">
        <img src="img/caf-1.jpg"/>
        <div>
            <img src="img/logo/Logo_Caf.svg" class="width40"/>
            <h2>Rapport d’activité</h2>
            <h3>2016</h3>
            <p>Print<p>
        </div>
    </a>
    <a href="index.php?page=caf&project=3" class="imgLink">
        <img src="img/caf-3.jpg"/>
        <div>
            <img src="img/logo/Logo_Caf.svg" class="width40"/>
            <h2>“Projet’toi”</h2>
            <h3>Identité graphique</h3>
            <p>Print Web<p>
        </div>
    </a>
    <a href="index.php?page=caf&project=4" class="imgLink">
        <img src="img/caf-4.jpg"/>
        <div>
            <img src="img/logo/Logo_Caf.svg" class="width40"/>
            <h2>La Semaine de la parentalité</h2>
            <h3>2017</h3>
            <p>Print Web</p>
        </div>
    </a>
</div>

