<!-- valtex group -->

<div id="galery">
    <a href="index.php?page=valtex&project=4" class="imgLink">
        <img src="img/valtex-4.jpg"/>
        <div>
            <img src="img/logo/Logo_Daycollection.svg" class="width65"/>
            <h2>Site web Daycollection</h2>
            <h3>Bannières</h3>
            <p>Web<p>
        </div>
    </a>
    <a href="index.php?page=valtex&project=1" class="imgLink">
        <img src="img/valtex-1.jpg"/>
        <div>
            <img src="img/logo/Logo_Daycollection.svg" class="width65"/>
            <h2>Catalogue Daycollection</h2>
            <h3>2013-2014</h3>
            <p>Print<p>
        </div>
    </a>
    <a href="index.php?page=valtex&project=2" class="imgLink">
        <img src="img/valtex-2.jpg"/>
        <div>
            <img src="img/logo/Logo_UnRdvFr.svg" class="width45"/>
            <h2>Un rendez-vous français</h2>
            <h3>Catalogue 2013-2014</h3>
            <p>Print<p>
        </div>
    </a>
    <a href="index.php?page=valtex&project=3" class="imgLink">
        <img src="img/valtex-3.jpg"/>
        <div>
            <img src="img/logo/Logo_UnRdvFr.svg" class="width45"/>
            <h2>Un rendez-vous élégant</h2>
            <h3>Impression textile</h3>
            <p>Print<p>
        </div>
    </a>
</div>

