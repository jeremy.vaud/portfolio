<!-- trendstop -->

<div id="project">
    <div class="company">
        <img src="img/logo/Logo_Trendstop.svg"/>
        <p>Bureau de style en ligne basé à Londres, Trendstop analyse et prévoit les tendances en matière de mode à l’international.</p>
    </div>
    <div class="picto">
        <img src="img/picto/Picto_Web.svg"/>       
    </div>
    <h2>Moodboards</h2>
    <h3>Tendances 2014-2015</h3>
    <p>Conceptions de planches de tendance à partir d’éléments fournis.</p>
    <img src="img/project/trendstop/Maquette-site_Moodboards_01.jpg"/>
    <img src="img/project/trendstop/Maquette-site_Moodboards_02.jpg"/>
    <img src="img/project/trendstop/Maquette-site_Moodboards_03.jpg"/>
    <img src="img/project/trendstop/Maquette-site_Moodboards_04.jpg"/>
    <img src="img/project/trendstop/Maquette-site_Moodboards_05.jpg"/>
</div>

