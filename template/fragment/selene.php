<!-- sélène et gaia -->

<div id="project">
    <div class="company">
        <img src="img/logo/Logo_SetG.svg"/>
        <p>Petite entreprise lyonnaise, Sélène & Gaïa crée du linge de lit pour toute la famille dans un
univers graphique et coloré.</p>
    </div>
    <div class="picto">
        <img src="img/picto/Picto_Print.svg"/>       
    </div>
    <h2>Linge de lit</h2>
    <h3>Conception graphique</h3>
    <p>Création de motifs pour une impression à cadres rotatifs.</p>
    <img src="img/project/selen/SeleneEtGaia_01.jpg"/>
    <img src="img/project/selen/SeleneEtGaia_02.jpg"/>
    <img src="img/project/selen/SeleneEtGaia_03.jpg"/>
    <img src="img/project/selen/SeleneEtGaia_04.jpg"/>
    <img src="img/project/selen/SeleneEtGaia_05.jpg"/>
    <img src="img/project/selen/SeleneEtGaia_06.jpg"/>
    <img src="img/project/selen/SeleneEtGaia_07.jpg"/>
    <img src="img/project/selen/SeleneEtGaia_08.jpg"/>
</div>

