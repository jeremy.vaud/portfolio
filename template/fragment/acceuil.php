<!-- acceuil -->

<div id="galery">
    <a href="index.php?page=caf&project=2" class="imgLink">
        <img src="img/caf-2.jpg"/>
        <div>
            <img src="img/logo/Logo_Caf.svg" class="width40"/>
            <h2>“Être locataire”</h2>
            <h3>Guide pratique</h3>
            <p>Print</p>
        </div>
    </a>
    <a href="index.php?page=laSouris" class="imgLink">
        <img src="img/laSouris.jpg"/>
        <div>
            <img src="img/logo/Logo_LaSouris.svg" class="width40"/>
            <h2>La souris apprivoisée</h2>
            <h3>logo et carte de visite</h3>
            <p>Identité visuelle</p>
        </div>
    </a>
    <a href="index.php?page=selene" class="imgLink">
        <img src="img/selen.jpg"/>
        <div>
            <img src="img/logo/Logo_SetG.svg" class="width40"/>
            <h2>Linge de lit</h2>
            <h3>Conception graphique</h3>
            <p>Print</p>
        </div>
    </a>
    <a href="index.php?page=csf" class="imgLink">
        <img src="img/csf.jpg"/>
        <div>
            <img src="img/logo/Logo_CSF.jpg" class="width40"/>
            <h2>Illustrations</h2>
            <h3>2015</h3>
            <p>Web</p>
        </div>
    </a>
    <a href="index.php?page=valtex&project=3" class="imgLink">
        <img src="img/valtex-3.jpg"/>
        <div>
            <img src="img/logo/Logo_UnRdvFr.svg" class="width65"/>
            <h2>Un rendez-vous élégant</h2>
            <h3>Impression textile</h3>
            <p>Print</p>
        </div>
    </a>
    <a href="index.php?page=kidiliz&project=3" class="imgLink">
        <img src="img/kidiliz-3.jpg"/>
        <div>
            <img src="img/logo/Logo_Kidiliz.svg" class="width45"/>
            <h2>Vitrine G-Star Raw</h2>
            <h3>Printemps-Été 2020</h3>
            <p>Print Web</p>
        </div>
    </a>
    <a href="index.php?page=kidiliz&project=2" class="imgLink">
        <img src="img/kidiliz-2.jpg"/>
        <div>
            <img src="img/logo/Logo_KidilizGroup.svg" class="width45"/>
            <h2>Pictogrammes</h2>
            <h3>Plateforme de vente B2B</h3>
            <p>Web</p>
        </div>
    </a>
    <a href="index.php?page=kidiliz&project=1" class="imgLink">
        <img src="img/kidiliz-1.jpg"/>
        <div>
            <img src="img/logo/Logo_Chipie.svg" class="width45"/>
            <h2>Workbook Chipie</h2>
            <h3>Printemps-Été 2020</h3>
            <p>Print</p>
        </div>
    </a>
    <a href="index.php?page=caf&project=1" class="imgLink">
        <img src="img/caf-1.jpg"/>
        <div>
            <img src="img/logo/Logo_Caf.svg" class="width40"/>
            <h2>Rapport d’activité</h2>
            <h3>2016</h3>
            <p>Print</p>
        </div>
    </a>
    <a href="index.php?page=kidiliz&project=4" class="imgLink">
        <img src="img/kidiliz-4.jpg"/>
        <div>
            <img src="img/logo/Logo_Kidiliz.svg" class="width45"/>
            <h2>Vitrine Color Block</h2>
            <h3>Printemps-Été 2020</h3>
            <p>Print Web</p>
        </div>
    </a>
    <a href="index.php?page=kidiliz&project=5" class="imgLink">
        <img src="img/kidiliz-5.jpg"/>
        <div>
            <img src="img/logo/Logo_Kidiliz.svg" class="width45"/>
            <h2>Tote bag de Noël</h2>
            <h3>Automne-Hiver 2020</h3>
            <p>Print</p>
        </div>
    </a>
    <a href="index.php?page=valtex&project=4" class="imgLink">
        <img src="img/valtex-4.jpg"/>
        <div>
            <img src="img/logo/Logo_Daycollection.svg" class="width65"/>
            <h2>Site web Daycollection</h2>
            <h3>Bannières</h3>
            <p>Web</p>
        </div>
    </a>
    <a href="index.php?page=valtex&project=1" class="imgLink">
        <img src="img/valtex-1.jpg"/>
        <div>
            <img src="img/logo/Logo_Daycollection.svg" class="width65"/>
            <h2>Catalogue Daycollection</h2>
            <h3>2013-2014</h3>
            <p>Print</p>
        </div>
    </a>
    <a href="index.php?page=caf&project=3" class="imgLink">
        <img src="img/caf-3.jpg"/>
        <div>
            <img src="img/logo/Logo_Caf.svg" class="width40"/>
            <h2>“Projet’toi”</h2>
            <h3>Identité graphique</h3>
            <p>Print Web</p>
        </div>
    </a>
    <a href="index.php?page=valtex&project=2" class="imgLink">
        <img src="img/valtex-2.jpg"/>
        <div>
            <img src="img/logo/Logo_UnRdvFr.svg" class="width65"/>
            <h2>Un rendez-vous français</h2>
            <h3>Catalogue 2013-2014</h3>
            <p>Print</p>
        </div>
    </a>
    <a href="index.php?page=caf&project=4" class="imgLink">
        <img src="img/caf-4.jpg"/>
        <div>
            <img src="img/logo/Logo_Caf.svg" class="width40"/>
            <h2>La Semaine de la parentalité</h2>
            <h3>2017</h3>
            <p>Print Web</p>
        </div>
    </a>
    <a href="index.php?page=trendstop" class="imgLink">
        <img src="img/trendstop.jpg"/>
        <div>
            <img src="img/logo/Logo_Trendstop.svg" class="width45"/>
            <h2>Moodboards</h2>
            <h3>Tendances 2014-2015</h3>
            <p>Web</p>
        </div>
    </a>
</div>

