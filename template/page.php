<!-- page principale -->

<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <link rel="icon" type="image/png" href="img/logo/favicon.png" />
        <title>Manon Nauton</title>
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Josefin+Sans:wght@100;300;400;700&display=swap" rel="stylesheet"> 
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <header id="header">
            <a href="index.php"><img src="img/picto/Picto_Home.svg"/></a>
            <a href="index.php" id="logo" class="<?=$logoSize?>"><img src="img/logo/Logo_Baniere.svg"/></a>
            <nav>
                <a href="https://www.linkedin.com/in/manon-nauton" target="_blank"><img src="img/picto/Picto_Linke.svg"/></a>
                <a href="pdf/CV_ManonNauton.pdf" target="_blank"><img src="img/picto/Picto_CV.svg"/></a>
                <a href="mailto:manon.nauton@gmail.com"><img src="img/picto/Picto_Mail.svg"/></a>
            </nav>
            <img id="menu" src="img/picto/Picto_Burger.svg"/>
        </header>
        <main id="main">
            <nav id="leftNav">
                <div>
                    <a href="index.php?page=kidiliz" class="projectLink">KIDILIZ GROUP</a>
                    <p id="kidiliz">PRÊT-À-PORTER ENFANT</p>
                </div>
                <div>
                    <a href="index.php?page=caf" class="projectLink">CAF</a>
                    <p id="caf">CAISSE D’ALLOCATIONS FAMILIALES DE CLERMONT-FD</p>
                </div>
                <div>
                    <a href="index.php?page=laSouris" class="projectLink">LA SOURIS APPRIVOISÉE</a>
                    <p id="laSouris">RÉPARATION INFORMATIQUE</p>
                </div>
                <div>
                    <a href="index.php?page=selene" class="projectLink">SÉLÈNE & GAÏA</a>
                    <p id="selene">LINGE DE LIT</p>
                </div>
                <div>
                    <a href="index.php?page=valtex" class="projectLink">VALTEX GROUP</a>
                    <p id="valtex">IMPRESSION TEXTILE</p>
                </div>
                <div>
                    <a href="index.php?page=trendstop" class="projectLink">TRENDSTOP</a>
                    <p id="trendstop">BUREAU DE TENDANCE EN LIGNE</p>
                </div>
                <div>
                    <a href="index.php?page=csf" class="projectLink">LA CSF</a>
                    <p id="csf">CONFÉDÉRATION SYNDICALE DES FAMILLES</p>
                </div>
            </nav>
            <?=$fragment?>
        </main>          
        <img src="img/taches/Tache_bleue.svg" id="blue"/>
        <img src="img/taches/Tache_jaune.svg" id="yellow"/>
        <img src="img/taches/Tache_rose.svg" id="pink"/>
        <a href="#top" id="btnUp"><img src="img/picto/Picto_Up.svg"/></a>
        <div class="test"></div>
        <footer id="footer">
            <div>
                <div>
                    <img src="img/picto/Picto_MailWhite.svg"><span>manon.nauton@gmail.com</span>
                </div>
                <span>Design : Manon Nauton | Développement et intégration : Jérémy Vaud - jeremy.vaud@gmail.com</span>
            </div>
        </footer>           
    </body>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="js/animation.js" type="text/javascript"></script>
</html>