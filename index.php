<?php

// Chargement d'une page en fonction de l'url

$logoSize = "large";
$file = 'template/fragment/acceuil.php';

if(isset($_GET['page'])){
    $page = $_GET['page'];
    if(isset($_GET['project'])){
        $project = intval($_GET['project']);
        if(file_exists('template/fragment/project/'.$page.'/project-'.$project.'.php')){
            $logoSize = "small";
            $file = 'template/fragment/project/'.$page.'/project-'.$project.'.php';
            $prev = $project - 1;
            $next = $project + 1;
            if($page === 'kidiliz'){
                if($project === 1){
                    $prev = 5;
                }elseif ($project === 5) {
                    $next = 1;
                }
            }else if($page === 'valtex' || $page="caf"){
                if($project === 1){
                    $prev = 4;
                }elseif ($project === 4) {
                    $next = 1;
                }
            }else{
                if($project === 1){
                    $prev = 3;
                }elseif ($project === 3) {
                    $next = 1;
                }
            }
            ob_start();
            include 'template/fragment/projectButton.php';
            $projectButton = ob_get_contents();
            ob_end_clean();
        }
    }else if(file_exists('template/fragment/'.$page.'.php')){
        if($page === 'laSouris' || $page === 'selene' || $page === 'csf' || $page === 'trendstop'){
            $logoSize = "small";
        }
        $file = 'template/fragment/'.$page.'.php';
    }  
}

ob_start();
include $file;
$fragment = ob_get_contents();
ob_end_clean();

include 'template/page.php';